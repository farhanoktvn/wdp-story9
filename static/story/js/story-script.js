$('#search-submit').on('click', function(){
  var input = $("#search").val();
  var api_url = "https://www.googleapis.com/books/v1/volumes?q=";
  $(".book-table").show();

  $.ajax({
    url: api_url + input,
    dataType: "jsonp",
    success: function(results) {
      var table_data = "";
      $.each(results.items, function(i, value) {
        var index = i + 1;
        table_data +=
          '<tr><th scope="row">' + index + "</th>" +
          '<td id="bookTitle">' + results.items[i].volumeInfo.title + "</td>" +
          '<td>' + results.items[i].volumeInfo.publishedDate + "</td>" +
          '<td id="bookLikes">' + '<p id="likesCount">' + 0 + "</p>" +
          '<button class="btn-like" id="btn-like">Like<i class="fa fa-thumbs-up ml-1"></i></button>' +
          '<button class="btn-unlike" id="btn-unlike">Meh<i class="fa fa-thumbs-down ml-1"></i></button>' +
          "</td>" +
          "</tr>"
      });
      $("#books").html(table_data);
    }
  })

  $(window).scrollTop($('#bookTable').position().top);
});

$(document).on('click', '#btn-like', function(){
  var likes = $(this).parent().children("p");
  var count = parseInt(likes.html()) + 1;
  likes.html(count)
});

$(document).on('click', '#btn-unlike', function(){
  var likes = $(this).parent().children("p");
  var count = parseInt(likes.html()) - 1;
  if (count < 0){
    likes.html(0);
  } else {
    likes.html(count)
  }
});

$('#most-liked').on('click', function(){
  var modalBody = $(".modal-body");
  var table = $("#bookList").clone();
  var tbody = sortTable(table.children("tbody"));
  table.children("tbody").remove();
  table.find("th:eq(2)").remove();
  tbody.find("tr").each(function(index) {
    $(this).find("td:eq(1)").remove();
    $(this).children("#bookLikes").children("button").remove();
    $(this).children("th").html(index+1);
  });
  tbody.find("tr:gt(4)").each(function() {
    $(this).remove();
  });
  table.append(tbody);
  modalBody.html(table);
});

function sortTable(tbody) {
  var rows = tbody.find('tr').toArray().sort(function(a, b) {
      var valA = getValue(a), valB = getValue(b);
      return parseInt(valB) - parseInt(valA);
  });
  for (var i = 0; i < rows.length; i++) {
    tbody.append(rows[i]);
  };
  return tbody;
}

function getValue(row) {
  return $(row).children("td#bookLikes").children("p#likesCount").text();
}
