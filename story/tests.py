from django.test import TestCase, Client
from django.urls import resolve
from django.contrib.staticfiles.testing import StaticLiveServerTestCase
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options

from story.views import index
import time

class StoryTest(TestCase):

    def test_index_page_response_success(self):
        response = Client().get('')
        self.assertEqual(response.status_code, 200)

    def test_index_page_use_index_template(self):
        response = Client().get('')
        self.assertTemplateUsed(response, 'index.html')

    def test_index_page_use_index_function(self):
        found = resolve('/')
        self.assertEqual(found.func, index)

class BookSearchTest(StaticLiveServerTestCase):

    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')

        self.browser = webdriver.Chrome(chrome_options=chrome_options)
        self.browser.get('http://localhost:8000')

    def tearDown(self):
        self.browser.close()

    def test_index_use_the_index_template(self):
        self.assertEqual('Story 9', self.browser.title)

    def test_search_display_book_list(self):
        search_bar = self.browser.find_element_by_id('search')
        search_bar.send_keys("advanced")
        self.browser.find_element_by_id("search-submit").click()
        time.sleep(5)
        self.assertIn("Advanced Computer Architecture and Computing",
            self.browser.page_source)

    def test_increment_and_decrement_likes(self):
        self.browser.find_element_by_id('search').send_keys("advanced")
        self.browser.find_element_by_id("search-submit").click()
        time.sleep(5)

        likes = self.browser.find_elements_by_id("likesCount")[0].text
        self.assertEqual(likes, "0")

        self.browser.find_elements_by_id("btn-like")[0].click()
        likes = self.browser.find_elements_by_id("likesCount")[0].text
        self.assertEqual(likes, "1")

        self.browser.find_elements_by_id("btn-unlike")[0].click()
        likes = self.browser.find_elements_by_id("likesCount")[0].text
        self.assertEqual(likes, "0")

    def test_modal_shows_sorted_rows(self):
        self.browser.find_element_by_id('search').send_keys("advanced")
        self.browser.find_element_by_id("search-submit").click()
        time.sleep(5)

        like_buttons = self.browser.find_elements_by_id("btn-like")
        for i in range(5):
            for j in range(i, 5):
                self.browser.find_elements_by_id("btn-like")[j].send_keys(Keys.RETURN)

        titleOld = self.browser.find_elements_by_id("bookTitle")[4].text
        self.browser.find_element_by_id("most-liked").click()
        time.sleep(3)

        titleNew = self.browser.find_elements_by_id("bookTitle")[10].text
        self.assertEqual(titleOld, titleNew)
