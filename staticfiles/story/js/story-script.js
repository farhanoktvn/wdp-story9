$('#search-submit').on('click', function(){
  var input = $("#search").value;
  var api_url = "https://www.googleapis.com/books/v1/volumes?q=";
  $(".book-table").show();

  $.ajax({
    url: api_url + input,
    dataType: "jsonp",
    success: function(results) {
      console.log(results)
    }
  })

});
